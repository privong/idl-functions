function maxarray,array1,array2
; element-by-element max() of two arrays

if (n_elements(array1) eq n_elements(array2)) then begin
  ; arrays are the same length, continue
  elemen=n_elements(array1)
  final=findgen(elemen)
  for i=0,elemen-1 do begin
    if (array1[i] gt array2[i]) then final[i]=array1[i]
    if (array1[i] le array2[i]) then final[i]=array2[i]
  endfor
endif else begin
  print,"Error: arrays are not the same length!"
endelse
return,final
end
