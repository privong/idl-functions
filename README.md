IDL Functions
------------------
Potentially useful IDL functions. **No longer maintained**

* histogram_discrete.pro -- discrete histogram when the values are not evenly spaced (e.g., model parameters)
* maxarray.pro -- returns element-wise max() of two arrays
* minarray.pro -- returns element-wise min() of two arrays