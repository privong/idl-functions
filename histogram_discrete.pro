function histogram_discrete,values,locations=locations

; histogram_discrete
;
; Used for histograms of arrays where you know there are only discrete values
; that may not be evenly spaced. (i.e. for gridded model parameters)
;
; usage:
; hist=histogram_discrete(values,locations=locations)
;
; histogram_distribution returns an array of number in each bin.
;
; The locations optional keyword returns an array with the values of the bins

; initialize some values
locations=0
hist=0

; sort the incoming values array
sindex=sort(values[*])
svalues=values[sindex]

for i=0,n_elements(svalues)-1 do begin
  if (i ne 0) then begin
    if (svalues[i] ne svalues[i-1]) then begin
      ; moving to a new value, add it in
      locations=[locations,svalues[i]]
      hist=[hist,1]
      j=j+1
    endif else begin
      ; we have the same value
      hist[j]=hist[j]+1
    endelse
  endif else begin
    locations=svalues[i]
    hist=1
    j=0
  endelse
endfor

return,hist
end
